var del = require('del');
var vinylPaths = require('vinyl-paths');

module.exports = function (options) {
	var paths = options.paths;

	gulp.task('clean', function () {
		return gulp.src([paths.tmp, paths.dist])
			.pipe(vinylPaths(del));
	});

};
