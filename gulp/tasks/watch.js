var reload = browserSync.reload;

module.exports = function (options) {
	function reportChange(event) {
		console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
	}

	gulp.task('watch', ['build'], function () {
		gulp.watch(options.files.jade, ['template', reload]).on('change', reportChange);
		gulp.watch(options.files.js, ['scripts', reload]).on('change', reportChange);
		gulp.watch([options.files.stylusAll], ['styles', browserSync.reload]).on('change', reportChange);
		gulp.watch(options.files.images, ['assets', browserSync.reload]).on('change', reportChange);
	});

};
