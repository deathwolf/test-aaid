var merge = require('merge-stream');

module.exports = function (options) {

	gulp.task('sprite', function () {

		
		var spriteData = gulp.src(options.files.sprite)
			.pipe($.plumber({errorHandler: options.errorHandler("sprite")}))
			.pipe($.spritesmith({
				imgName: 'sprite.png',
				cssName: 'sprite.styl',
				cssFormat: 'styl',
				algorithm: 'binary-tree',
				padding: 5,
				cssTemplate: 'stylus.template.mustache',
				cssVarMap: function (sprite) {
					sprite.name = 'ico-' + sprite.name
				}
			}));
		
		var imgStream = spriteData.img
			.pipe(gulp.dest(options.paths.src + '/images'));
		var cssStream = spriteData.css
			.pipe(gulp.dest(options.paths.src + '/styles/base/'));

		return merge(imgStream, cssStream)
	});

};